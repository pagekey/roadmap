# 1.0.0 (2024-03-29)


### Bug Fixes

* Add pagekey docgen ([f580206](https://gitlab.com/pagekey/website/commit/f5802060acea1f417cf8c4277de700ef94b3ccf2)), closes [#4](https://gitlab.com/pagekey/website/issues/4)
* Use common pipeline for next site generation ([fec437c](https://gitlab.com/pagekey/website/commit/fec437c5bef996f88f7d7f13a042b713f1003f64))
